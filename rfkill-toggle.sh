#!/bin/sh

connmanctl disable bluetooth

sleep 2

# run the command twice to ensure we're getting a disabled result...
out=$(connmanctl disable bluetooth 2>&1)
if ! echo "$out" | grep -i 'bluetooth.* already disabled'; then
	echo "rfkill-toggle: FAILED"
	echo "Unable to disable bluetooth"
	exit 1
fi

sleep 2

out=$(sudo hciconfig hci0 reset 2>&1)
if [ "$out" != "Can't init device hci0: Operation not possible due to RF-kill (132)" ]; then
	echo "rfkill-toggle: FAILED"
	echo "RF-kill did not prevent device reset"
	exit 1
fi

sleep 2

out=$(connmanctl enable bluetooth 2>&1)
if [ "$out" != "Enabled bluetooth" ]; then
	echo "rfkill-toggle: FAILED"
	echo "Unable to enable bluetooth"
	exit 1
fi

sleep 2

out=$(sudo hciconfig hci0 reset 2>&1)
if [ $? != 0 ]; then
	echo "rfkill-toggle: FAILED"
	echo "Device reset failed when it should succeed"
	exit 1
fi

echo "rfkill-toggle: PASSED"
exit 0
